#coding: utf8
import codecs
from encodings.big5 import codec
from os import listdir

from os.path import join, isfile
from __builtin__ import type
import bs4 as bs
from turtle import _screen_docrevise
import xml.etree.ElementTree as ET


__author__ = 'vandermonde'

#from __future__ import unicode_literals



from hazm import Normalizer, word_tokenize, Stemmer
from sets import Set

#TODO init: fill categories list, define base dir, define stop words file, put each directory files in: base dir/category name, rFactor, threshold

#categories structure (cat name, number of files in cat, {word: [doc count,vazn]})
categories = [ ]
#words structure [['word',vazn,category],...]
wordsStruct = [[]]

baseDir = "../test"

rFactor = 1
threshold = 1

def readStopWords(filename):
    """returns words from files in filename"""
    words = []
    with codecs.open(filename,'r',"utf8") as stopWordsFile:
        words = stopWordsFile.readlines()

    #TODO scrutiny effect of this block

    for i in range(len(words)):
        words[i] = words[i].strip()


    #stemmer = Stemmer()
    #normalizer = Normalizer()
    #for i in range(len(words)):
#        words[i] = normalizer.normalize(words[i])
 #       words[i] = stemmer.stem(words[i]).strip()

    return words


def readSymbols(filename):
    """returns words from files in filename"""
    words = []
    with codecs.open(filename,'r',"utf8") as stopWordsFile:
        words = stopWordsFile.readlines()

    #TODO scrutiny effect of this block

    for i in range(len(words)):
        words[i] = words[i].strip()


    #stemmer = Stemmer()
    #normalizer = Normalizer()
    #for i in range(len(words)):
#        words[i] = normalizer.normalize(words[i])
 #       words[i] = stemmer.stem(words[i]).strip()


    return words



stopWords = readStopWords(baseDir+"/"+"stopwords.txt" )
symbols  = readSymbols(baseDir+ "/" + "symbols.txt")



def getPathFiles(path):
    """gets a path and returns a list of files in the path"""
    files = []
    files = [file for file in listdir(path) if isfile(join(path,file))]
    return  files


def readXmlFile(path):
    """read an xml data file in crtf format returning each text for a subject in following format: [[category,text],...]"""

    categoriesText = []
    #TODO check any cahnge at this point
    with codecs.open(path,'r',"utf8") as file:
        soup = bs.BeautifulSoup(file,'xml')
        docs = soup.find_all("DOC")

        for doc in docs:
            category = doc.CAT.text

            images = doc.find_all("IMAGE")
            for image in images:
                image.decompose()

            catText = doc.TEXT.text


            categoriesText.append([category, catText])

    return categoriesText




def extractWord(text):
    """returns a set of normalized and stemmed words removing stop words from a text"""

    normalizer = Normalizer()
    #normalizedText = normalizer.normalize(text)

    #TODO check effect of sentTokenize
    #wordTokens = word_tokenize(normalizedText)

    for symbol in symbols:
        text = text.replace(symbol,' ')

    wordTokens = text.split()

    #stemmer = Stemmer()
    stemmedWords = Set()
    for i in range(len(wordTokens)):
        #stemmedWords.add( (stemmer.stem(wordTokens[i])).strip() )
        stemmedWords.add(wordTokens[i].strip())

    finalWords = set()

    for word in stemmedWords:

        if(word not in stopWords):
            finalWords.add(word)
       # else:
        #    print "deleted word: " + word

    #TODO check effect of lemmatizer

    return finalWords

def findCategoryNomByName(name):
    for i in range(len(categories)):
        if(categories[i][0] == name):
            return i

    return -1

def countDirectoryWords(directoryPath):
    """read all words in all files at category path and sets count of each in categories data structure"""
    directoryFiles = getPathFiles(directoryPath)
    for directoryFile in directoryFiles:
        fileText = readXmlFile(directoryPath+"/"+directoryFile)

        for categoryText in fileText:

            filewords = extractWord(categoryText[1])

            catNom = findCategoryNomByName(categoryText[0])
            if(catNom == -1):
                catNom = len(categories)
                categories.append([categoryText[0],1,{}])
            else:
                #print "catnom increased"
                categories[catNom][1] += 1
            #print "////////////////////////////////////////////////////////////////////////////////////"
            #print "category:" + categoryText[0] + " number: " + str(catNom) + " count: " + str(categories[catNom][1])

            for word in filewords:
                if(word in categories[catNom][2].keys()):
                    categories[catNom][2][word][0] += 1
                else:
                    categories[catNom][2][word]=[1]
            #print "######################################################################################"




def calculateNegFactor(catNom, word):
    totaldocs = 0;
    wordCount = 0;


    for categoryNumber in range(len(categories)):
        if(categoryNumber == catNom):
            continue

        else:
            totaldocs += categories[categoryNumber][1]

            if(word in categories[categoryNumber][2].keys()):
                wordCount += categories[categoryNumber][2][word][0]


    return float(wordCount)/totaldocs


def evaluateCategoryWords(catNom):
    """calculate weight for each word in the category"""
    for word in categories[catNom][2].keys():
        if(categories[catNom][2][word][0]> threshold):
            posFactor = float(categories[catNom][2][word][0])/categories[catNom][1]
            #print str(categories[catNom][2][word][0]) + "/"+ str(categories[catNom][1])
            negFactor = calculateNegFactor(catNom,word)
            #print str(posFactor) + " : " + str(negFactor)



            weight = posFactor/(negFactor + 0.01)




            categories[catNom][2][word].append(1 - ( rFactor/(rFactor+weight) ))


def learn():
    """put files at baseDir/corpus  needs some changes if more than one directory """
    print "start to learn"
    #for i in range(len(categories)):

    directoryPath = baseDir+"/"+ "corpus"

    countDirectoryWords( directoryPath)



    for i in range(len(categories)):
        evaluateCategoryWords(i)

    print "counted words: "
    for category in categories:
        print(category[0] + ":  " +  str(category[1]) )

        for word in category[2].iteritems():
            if( word[1][0] >1 ):
                if(word[1][1]>0.96):
                    print word[0] + str(word[1])


        print "\n################################################################################\n"



learn()



















